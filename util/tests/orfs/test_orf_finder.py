from collect_annotations import Genome, OrfFinder, OrfAnnotator


def test_find_orfs_both_strands():
    test_genome_path = "test_genome.fsa"
    genome = Genome(test_genome_path)
    orf_finder = OrfFinder(genome=genome, min_len=6)
    orfs = orf_finder.collect_ORFs()
    expected_value = "[id: 1, start: 10, stop: 33, length: 24, sequence: ATGTTTTTTATGTTTTTTTTTTAA, strand: +, id: 2, start: 49, stop: 66, length: 18, sequence: ATGTTTTTTTTTTTTTGA, strand: +, id: 3, start: 103, stop: 117, length: 15, sequence: ATGATGATGTTTTAA, strand: +, id: 4, start: 98, stop: 84, length: 15, sequence: ATGAAAAAAAAATGA, strand: -, id: 5, start: 87, stop: 73, length: 15, sequence: ATGAAAAAAAAATGA, strand: -, id: 6, start: 76, stop: 47, length: 30, sequence: ATGAAAAAAATCAAAAAAAAAAAAACATAA, strand: -]"

    assert len(orfs) == 6
    assert str(orfs) == expected_value

def test_find_orfs_overlap_on_single_strand():
    test_genome_path = "test_genome_2.fsa"
    genome = Genome(test_genome_path)
    orf_finder = OrfFinder(genome=genome, min_len=6)
    orfs = orf_finder.collect_ORFs()
    expected_value = "[id: 1, start: 4, stop: 18, length: 15, sequence: ATGTTTTATGTTTAA, strand: +, id: 2, start: 11, stop: 22, length: 12, sequence: ATGTTTAATTAA, strand: +]"
    assert len(orfs) == 2
    assert str(orfs) == expected_value



if __name__ == "__main__":
    test_find_orfs_both_strands()
    # test_find_orfs_overlap_on_single_strand()