# 2017, Mathias Wajnberg

import os
import subprocess
import sys
from copy import deepcopy as deepcopy

from Bio import SeqIO
from Bio.Alphabet import IUPAC
from Bio.Blast import NCBIXML
from Bio.Blast.Applications import NcbiblastxCommandline
from Bio.Seq import Seq


def run_routine(path_to_viral_genome: str, collect_ORFs=True, min_len=93, run_blastp=True,
                path_to_metadata_db="./util/meta_data_db/poxviridae_cds_database_ncbi", preferred_refs = None):
    """
    Routine that can collect ORF positions in ".fsa" files, blasts that against a automatically created protein database
    and exports the annotated longest possible ORFS to a GenBank file. Unmappable ORFs are added to a different track.
    :param path_to_viral_genome: Viral genome in ".fsa" format. The same directory must contain .sbt file.
    :param collect_ORFs: Boolean, default = True
    :param min_len: minimum length of open reading frames
    :param run_blastp: Boolean, default = True. orfs must have been collected in this directory beforehand
    :param path_to_metadata_db: default= "./IOC_based_annotation/poxviridae_cds_database_ncbi"
    :return: None
    """

    print(os.path.dirname(os.path.realpath(__file__)))

    db_path = path_to_metadata_db
    if not path_to_viral_genome.endswith(".fsa"):
        print("Input genome must be in \".fsa\" format.")

    else:
        genome = Genome(input_path=path_to_viral_genome)
        genome_orfs = OrfFinder(genome, min_len=min_len)
        if collect_ORFs:
            genome_orfs.collect_ORFs()
            genome_orfs.write_orfs_to_file()
        if run_blastp:
            annotations = OrfAnnotator(orfs=genome_orfs, db_path=db_path, preferred_refs= preferred_refs)
            annotations.blastp_orfs()
            annotations.filter_blastp_result()
            annotations.filtered_blastp_tbl_to_genbank()


# helper functions
def in_frame(pos: int, pos2: int):
    left = min(pos, pos2)
    right = max(pos, pos2)
    in_frame = (right - left) % 3 == 0
    other_codons_inbetween = right >= left + 3

    return in_frame and other_codons_inbetween


class Genome:
    def __init__(self, input_path):
        self.input_path = input_path  # must be .fsa and in same dir as .sbt for tbl2asn
        self.seqIO_record = SeqIO.read(self.input_path, "fasta")  # get nucelotide sequence
        self.sequence = str(self.seqIO_record.seq)
        self.name = self.seqIO_record.name
        self.description = self.seqIO_record.description
        self.seqIO_reverse_complement = \
        [rec.reverse_complement(id="rc_" + rec.id, description="reverse complement") for rec in
         SeqIO.parse(self.input_path, "fasta")][0]
        self.rc_sequence = str(self.seqIO_reverse_complement.seq)


class Orf():
    def __init__(self, start, stop, length, id, sequence, strand):
        self.start = start
        self.stop = stop
        self.length = length
        self.id = id
        self.sequence = sequence
        self.strand = strand

    def __str__(self):
        return "id: {}, start: {}, stop: {}, length: {}, sequence: {}, strand: {}".format(
            self.id, self.start, self.stop, self.length, self.sequence, self.strand)

    def __repr__(self):
        return self.__str__()


class OrfFinder:
    """
    Considers nucelotide sequence of single genome from FASTA file (.fsa format) and collects ORF information.
    Finds longest possible ORFs.
    """

    def __init__(self, genome: Genome, start_codons=['ATG'], stop_codons=["TAA", "TAG", "TGA"], min_len=93,
                 output="output"):
        """
        Initializes object
        :param input_path: path to .fsa file
        :param start_codons: List of strings
        :param stop_codons: List of strings
        :param min_len: integer of minimum ORF length
        """
        self.genome = genome
        self.start_codons = start_codons  # DNA!
        self.stop_codons = stop_codons
        self.min_len = min_len
        self.orfs = []
        self.orf_id_helper = 1
        self.output_path = output
        self.orfs_file_path = self.output_path + "/" + self.genome.description + "_" + "ORFs.fna"
        self.orfs_AA_file_path = self.output_path + "/" + self.genome.description + "_" + "ORFs_AA.faa"

        self._validate()
        if not os.path.exists(self.output_path):
            os.mkdir(self.output_path)
        if not os.path.exists( self.output_path+"/"+os.path.basename(self.genome.input_path)):
            os.link(self.genome.input_path, self.output_path+"/"+os.path.basename(self.genome.input_path))

    def _validate(self):
        assert isinstance(self.start_codons, list)
        assert isinstance(self.start_codons[0], str)
        assert isinstance(self.stop_codons, list)
        assert isinstance(self.stop_codons[0], str)
        assert isinstance(self.min_len, int)
        for codon in self.start_codons + self.stop_codons:
            assert len(codon) == 3
        try:
            assert self.genome.input_path.endswith(".fsa")
        except AssertionError:
            print("Input file must be in .fsa format.")
            exit()

    def __print__(self):
        return '{} ORFs in  {}'.format(len(self.orfs), self.genome.input_path)

    def collect_ORFs(self):
        """
        Finds all stop codons and the respective furthest inframe start codon.
        They are added as a dictionaries to the object.
        :return: List of dictionaries holding ORF information. Position coordinates are zero-based.
        """

        stop_codon_positions = self.find_stop_codon_positions()
        print("Found ", len(stop_codon_positions), " stop codons.")
        self.find_longest_orfs(stop_codon_positions)
        print("Found ", len(self.orfs), " ORFs.")

        return self.orfs

    def find_stop_codon_positions(self):
        """
        Finds all stop codon positions in all frames on both strands.
        :return:
        """
        scanned_region = range(0, len(self.genome.sequence) - 2)
        forward_stop_codons = [{"pos": pos, "strand": "+"} for pos in scanned_region if
                               self.genome.sequence[pos:pos + 3]
                               in self.stop_codons]  # 0 based indices

        rc_stop_codons = [{"pos": pos, "strand": "-"} for pos in scanned_region if self.genome.rc_sequence[pos:pos + 3]
                          in self.stop_codons]  # 0 based indices

        all_stop_codons = forward_stop_codons + rc_stop_codons
        return all_stop_codons

    def find_longest_orfs(self, stop_codon_positions):
        """
        Processes stop codon information, in order to separate them by strand information.
        :param stop_codon_positions: List of dictionaries containing 1 based stop codon positions and strand information
        :return:
        """
        forward_stop_codon_positions = [dict["pos"] for dict in stop_codon_positions if dict["strand"] == "+"]
        self.get_orfs_from_stop_pos(self.genome.sequence, forward_stop_codon_positions)
        rc_stop_codon_positions = [dict["pos"] for dict in stop_codon_positions if dict["strand"] == "-"]
        self.get_orfs_from_stop_pos(self.genome.rc_sequence, rc_stop_codon_positions, strand="reverse_complement")

    def get_orfs_from_stop_pos(self, sequence, stop_codon_positions, strand=None):
        """
        For each given stop codon poistion, searchs for inframe start codon positions of the longest possible ORFs
        :param sequence: string of nucleotides
        :param stop_codon_positions: list of zero based index integers
        :param strand: empty = forward strand / "reverse_complement" = backward strand
        :return:
        """
        for stop_codon_pos_index in range(0, len(stop_codon_positions)):
            if stop_codon_positions[stop_codon_pos_index] >= self.min_len - 2:
                begin_for_start_codon_search = None
                start_codon_pos = None
                stop_codon_pos = stop_codon_positions[stop_codon_pos_index]  # is 0 based index
                if stop_codon_pos_index == 0:
                    begin_for_start_codon_search = 0 + stop_codon_positions[stop_codon_pos_index] % 3
                else:
                    for index in range(stop_codon_pos_index - 1, -1, -1):
                        # check if previously found stop codons are in frame
                        if in_frame(stop_codon_positions[stop_codon_pos_index], stop_codon_positions[index]):
                            begin_for_start_codon_search = stop_codon_positions[index]
                            break
                        else:
                            begin_for_start_codon_search = 0 + stop_codon_positions[stop_codon_pos_index] % 3

                if isinstance(begin_for_start_codon_search, int) \
                        and (stop_codon_pos + 2 - begin_for_start_codon_search >= self.min_len):
                    start_codon_pos = self.find_furthest_start_codon(sequence=sequence,
                                                                     startpoint=begin_for_start_codon_search,
                                                                     stoppoint=stop_codon_pos)

                if start_codon_pos and start_codon_pos >= 5900 and stop_codon_pos <= 6012:
                    pass
                if start_codon_pos and stop_codon_pos + 2 - start_codon_pos + 1 >= self.min_len \
                        and in_frame(start_codon_pos, stop_codon_pos):
                    self.add_orf_info_to_object(sequence=sequence, start_codon_pos=start_codon_pos,
                                                stop_codon_pos=stop_codon_pos, strand=strand)

    def find_furthest_start_codon(self, sequence, startpoint, stoppoint):
        """
        For a given stop codon position in a sequence, finds the furthest inframe start codon.
        :param sequence: string of nucleotides
        :param startpoint: integer index, zero based, builds the span in which to search for start codons, usually last
                           stop codon position
        :param stoppoint: integer index, zero based
        :return: integer index, zero based
        """
        scanned_region = range(startpoint, stoppoint, 3)  # zero based indices
        start_codon_positions_in_scanned_region = [pos for pos in scanned_region if sequence[pos:pos + 3]
                                                   in self.start_codons]  # return zero based index of inframe start codons

        if start_codon_positions_in_scanned_region:
            return min(
                start_codon_positions_in_scanned_region)  # we want the longest possible ORF: return first start codon
        else:
            return None

    def add_orf_info_to_object(self, sequence, start_codon_pos, stop_codon_pos, strand=None):
        """
        Adds succesfully found ORF information as dictionaries to object.
        Recalculates postions for reverse strand to relate to forward strand.
        Indices are now 1-based after this step.
        :param sequence: string of nucelotides
        :param start_codon_pos: integer
        :param stop_codon_pos: integer
        :param strand: strand information
        :return:
        """
        assert start_codon_pos < stop_codon_pos
        if not strand:
            strand = "+"
        orf = sequence[int(start_codon_pos):stop_codon_pos + 3]
        orf_length = len(orf)
        if strand == "reverse_complement":
            strand = "-"
            start_codon_pos = len(self.genome.sequence) - (start_codon_pos + 1)  # zero based index
            stop_codon_pos = len(self.genome.sequence) - (
            stop_codon_pos + 3)  # because zero based index on reverse strand and codon is part of orf
        else:
            stop_codon_pos = stop_codon_pos + 2  # the whole stop codon is part of the ORF
        assert orf_length % 3 == 0

        if orf_length >= (self.min_len):
            self.orfs.append(
                Orf(start=start_codon_pos + 1, stop=stop_codon_pos + 1, length=orf_length, id=self.orf_id_helper,
                    sequence=orf, strand=strand)
            )
            self.orf_id_helper += 1

    def write_orfs_to_file(self):
        seqIO_sequences = [SeqIO.SeqRecord(seq=Seq(orf.sequence, IUPAC.ambiguous_dna),
                                           id="ORF#" + str(orf.id) + "_" + str(orf.start) + "_" + str(orf.stop)
                                              + "_" + orf.strand, description="")
                           for orf in self.orfs]

        # Write ORF information to FASTA file
        SeqIO.write(seqIO_sequences, self.orfs_file_path, "fasta")
        print("Wrote \"" + self.orfs_file_path + "\" file containing ORF sequences. Position indices are zero based.")

        print("Translating ORFs to amino acid seqeunces.")
        prot_sequences = []

        for sequence in seqIO_sequences:
            as_seq = sequence.seq.transcribe().translate()
            prot_sequences.append(
                SeqIO.SeqRecord(
                    seq=as_seq,
                    id=sequence.id + "_AA",
                    description=""

                ))
        SeqIO.write(prot_sequences, self.orfs_AA_file_path, "fasta")
        print("Wrote \"" + self.orfs_AA_file_path + "\" file containing translated ORF sequences.")


class OrfAnnotator:
    """
    Takes ORF information from OrfFinder object and collects annotations from local database.
    Can create GenBank file.
    """

    def __init__(self, orfs: OrfFinder, db_path, preferred_refs=None, evalue=0.001,
                 metadata_path="util/meta_data_db/poxviridae_all_genomes_from_ncbi.gb"):
        self.orfs = orfs
        self.preferred_refs = preferred_refs
        self.db_path = db_path
        self.metadata_path = metadata_path
        self.evalue = evalue
        self.cleaned_up_blast = None
        self.blastp_raw_file_path = self.orfs.output_path + "/" + self.orfs.genome.description + "_" + "blastp_result_raw.xml"
        self.blastp_filtered_file_path = self.orfs.output_path + "/" + str(os.path.basename(
            self.orfs.genome.input_path).split(".")[0]) + ".tbl"
        self.validate()

    def validate(self):
        assert os.path.exists(os.path.dirname(self.db_path))

    def prepare_genbank_file_for_prot_db_creation(self):
        ignore_counter = 0
        with open(self.orfs.output_path + "/" + self.orfs.genome.name + "_translations.fa", 'w') as outfile:
            for seq in SeqIO.parse(self.metadata_path, 'genbank'):
                # gather infos
                accession_number = seq.annotations['accessions'][0]
                name = seq.description
                sequence = str(seq._seq)
                for feature in seq.features:
                    if feature.type == 'CDS':
                        try:
                            translation = feature.qualifiers["translation"][0]
                        except:
                            translation = ""
                        try:
                            protein_id = feature.qualifiers["protein_id"][0]
                        except KeyError:
                            protein_id = "-N/A-"
                        try:
                            feature_name = feature.qualifiers['standard_name'][0]
                        except KeyError:
                            try:
                                feature_name = feature.qualifiers['gene'][0]
                            except KeyError:
                                try:
                                    feature_name = feature.qualifiers['product'][0]
                                except KeyError:
                                    try:
                                        feature_name = feature.qualifiers['locus_tag'][0]
                                    except KeyError:
                                        feature_name = '-N/A-'
                        try:
                            position = [int(feature.location._start), int(feature.location._end)]
                        except AttributeError:
                            position = [int(feature.location.parts[0]._start), int(feature.location.parts[-1]._end)]

                        if position[0] > position[1]:
                            position = [position[1], position[0]]

                        # write out stuff
                        if translation:
                            title = '|'.join(
                                [accession_number, name.replace(' ', '_'), protein_id, feature_name.replace(' ', '_')])
                            outfile.write('>{}\n{}\n'.format(title, translation))
                        else:
                            ignore_counter += 1
        print("Wrote ", self.orfs.output_path + "/" + self.orfs.genome.name + " translations. Ignored " + str(
            ignore_counter) + " sequences as no translation information was available.")

    def create_protein_db(self):
        self.prot_db_title = self.orfs.genome.name + "_protDB"

        self.prepare_genbank_file_for_prot_db_creation()
        make_blast_db_command = "makeblastdb -in {} -input_type fasta -dbtype prot -out {}".format(
            self.orfs.output_path + "/" + self.orfs.genome.name + "_translations.fa",
            self.orfs.output_path + "/" + self.prot_db_title)
        print("Creating blast DB with the following command\n:", make_blast_db_command)
        subprocess.call(make_blast_db_command, shell=True)

    def blastp_orfs(self, evalue=None):
        if evalue:
            self.evalue = evalue
        self.create_protein_db()
        blastx_cline = NcbiblastxCommandline(cmd="blastp", db=self.orfs.output_path + "/" + self.prot_db_title,
                                             query=self.orfs.orfs_AA_file_path,
                                             evalue=self.evalue, outfmt=5, out=self.blastp_raw_file_path,
                                             num_threads=30, num_alignments=20)

        print("Running local protein blast with the following parameters:")
        print("\t", blastx_cline)
        blastx_cline()
        print("Wrote \"" + self.blastp_raw_file_path + "\" file containing raw blast results.")

    def filter_blastp_result(self):
        raw = open(self.blastp_raw_file_path)
        raw_records = NCBIXML.parse(raw)
        filtered_records = []
        evalues = None
        print("Applying filter on blast output...")
        if self.preferred_refs:
            assert type(self.preferred_refs == "list")
            print("... filtering preferred references (", self.preferred_refs, ") ...")
        for record in raw_records:
            smallest_evalue = None
            if record.alignments:
                for alignment in record.alignments:
                    if len(alignment.hsps) > 1:
                        # if there is more than one HSP, make sure they are sorted by e-value therefore later only the
                        # first HSP will be considered
                        alignment.hsps.sort(key=lambda x: x.expect)

                evalues = [alignment.hsps[0].expect for alignment in record.alignments]
            else:
                # no alignment information found. "Empty" record simply gets added
                filtered_records.append(record)
            if evalues:
                smallest_evalue = min(evalues)
            best_alignments = None
            if smallest_evalue != None:
                best_alignments = [alignment for alignment in record.alignments
                                   if alignment.hsps[0].expect <= smallest_evalue]
            if self.preferred_refs:
                smallest_evalue_ref = None
                best_alignments_ref = None
                for ref in reversed(self.preferred_refs):
                    evalues_ref = [alignment.hsps[0].expect for alignment in record.alignments
                                   if alignment.hit_def.startswith(ref)]
                    if evalues_ref:
                        smallest_evalue_ref = min(evalues_ref)
                    if smallest_evalue_ref:
                        best_alignments_ref = [alignment for alignment in record.alignments
                                               if alignment.hit_def.startswith(ref)
                                               and alignment.hsps[0].expect <= smallest_evalue_ref]
                        if best_alignments_ref and best_alignments:
                            best_alignments = best_alignments_ref + best_alignments
                            # else:
                            #     print("No blast result for reference ",ref, " for query ", record.query," found.")
            if best_alignments:
                filtered_record = deepcopy(record)  # will hold the fitlered info
                filtered_record.alignments = best_alignments
                filtered_records.append(filtered_record)

        print("...done")
        self.filtered_blastp_to_tbl(filtered_records)

    def filtered_blastp_to_tbl(self, records):
        if os.path.exists(self.blastp_filtered_file_path):
            print("Previous .tbl found! Deleting \"" + str(self.blastp_filtered_file_path) + "\"")
            os.remove(self.blastp_filtered_file_path)
        with open(self.blastp_filtered_file_path, "w") as tbl:
            tbl.write(">Features\t" + self.orfs.genome.description + "\tfiltered_blast_results" + "\n")
            for blast in records:
                start = blast.query.split("_")[1]
                stop = blast.query.split("_")[2]
                if blast.alignments:
                    tbl.write(start + "\t" + stop + "\tCDS" + "\n")
                    best_product = blast.alignments[0].title.split("|")[-1]
                    best_protein_id = blast.alignments[0].title.split("|")[-2]
                    tbl.write("\t\t\tproduct\t" + best_product + "\n")
                    tbl.write("\t\t\tprotein_id\t" + best_protein_id + "\n")
                    note_string = "\t\t\tnote\t"

                    alignments = []
                    for alignment in blast.alignments:
                        alignments.append({
                            "reference": alignment.hit_def.split("|")[0],
                            "gene": alignment.title.split("|")[-1],
                            "protein": alignment.title.split("|")[-2],
                            "similarity": round((alignment.hsps[0].identities / alignment.hsps[0].align_length),
                                                3) * 100,
                            # percent
                        })
                    references = [alignment.hit_def.split("|")[0] for alignment in blast.alignments]
                    if self.preferred_refs:
                        for ref_index in range(0, len(self.preferred_refs)):
                            if self.preferred_refs[ref_index] not in references:
                                no_info_dict = {
                                    "reference": self.preferred_refs[ref_index],
                                    "gene": "NA",
                                    "protein": "NA",
                                    "similarity": "NA"
                                }
                                if ref_index == 0:
                                    alignments[1:] = alignments[0:-1]
                                    alignments[0] = no_info_dict
                                else:
                                    if len(alignments) < ref_index + 1:
                                        alignments.append(no_info_dict)
                                    else:
                                        alignments[ref_index + 1:] = alignments[ref_index:]
                                        alignments[ref_index] = no_info_dict

                        alignments = sorted(alignments, key=self.give_alignment_for_sorting)

                    for alignment_dict in alignments:
                        note_string += ("Reference:" + alignment_dict["reference"])
                        note_string += (", Gene:" + alignment_dict["gene"])
                        note_string += (", Protein:" + alignment_dict["protein"])
                        note_string += (", Similarity:" + str(alignment_dict["similarity"]) + "; ")

                    tbl.write(note_string + "\n")
                else:
                    tbl.write(start + "\t" + stop + "\tmisc_feature" + "\n") # not found ORFs will be put on the misc_feature track only for distinction purposes
                    tbl.write("\t\t\tnote\t" + blast.query[:-3] + "\n")

        print("Wrote \"" + self.blastp_filtered_file_path + "\" file containing filtered blast results.")

    def give_alignment_for_sorting(self, alignment):
        for index, pref_ref in enumerate(self.preferred_refs):
            if alignment["reference"] in pref_ref:
                return index
        return len(self.preferred_refs)

    def filtered_blastp_tbl_to_genbank(self):
        print("Starting GenBank file generation.")
        subdirs = [x[0] for x in os.walk(".")]
        print("\tSearching for .tbl and .sbt template file...", end="")

        template_files = []
        template = None
        tbl_files = []
        for dir in subdirs:
            for file in os.listdir(dir):
                if file.endswith(".sbt"):
                    template_files.append(dir + "/" + file)
                elif file.endswith(".tbl"):
                    tbl_files.append(file)

        if template_files:
            if len(template_files) > 1:
                print("\n\tMultiple template files found in this directory:")
                for temp in range(0, len(template_files)):
                    print("\t", temp, ": \t", template_files[temp])
                print("Using the first one (default choice).")
                choice = 0
                template = template_files[int(choice)]
                print("\tUsing 0:", template)
            else:
                template = template_files[0]
                print("\tfound. \n\tUsing", template)
        else:
            print("\tNo template file provided. You can create one under this URL: ")
            print("\t\t https://submit.ncbi.nlm.nih.gov/genbank/template/submission/")
            print(
                "\tSave it in this directory or a subdirectory and rerun this step of the routine, by calling the \"" +
                sys._getframe().f_code.co_name + "\" method!"
                )
        if not tbl_files:
            print("\tno .tbl file was found. Remember creating it with the respective method: filter_blastp_result()")
        elif template:
            print(".tbl file found.")
            tbl2asn_call = " ".join(
                ["util/tbl2asn/tbl2asn.2.6.32", "-p", "./"+self.orfs.output_path, "-t", str(template), "-V b -r output"])
            print("Generating GenBank file using \"tbl2sn\":")
            print("\t", tbl2asn_call)
            subprocess.call(tbl2asn_call, shell=True)

        gbf_there = False
        sqn_there = False
        subdirs = [x[0] for x in os.walk(".")]
        for dir in subdirs:
            for file in os.listdir(dir):
                if file.endswith(".gbf"):
                    gbf_there = True
                elif file.endswith(".sqn"):
                    sqn_there = True

        if gbf_there and sqn_there:
            print("GenBank files successfully created.")
        else:
            print("GenBank files were not created.")
